//
//  ViewController.swift
//  SastaTicket
//
//  Created by Haris on 09/05/2024.
//

import UIKit
import Alamofire


class ViewController: UIViewController {
    
    lazy var busButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Bus Ticketing", for: .normal)
        btn.backgroundColor = .blue
        btn.layer.cornerRadius = 10
        btn.tag = 1
        btn.addTarget(self, action: #selector(didTappedButton), for: .touchUpInside)
        return btn
    }()
    lazy var airPlaneButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Air Ticketing", for: .normal)
        btn.backgroundColor = .blue
        btn.layer.cornerRadius = 10
        btn.tag = 2
        btn.addTarget(self, action: #selector(didTappedButton), for: .touchUpInside)
        return btn
    }()
    private let stackView: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.spacing = 12
        stack.distribution = .fillEqually
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home"
        navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .white
        setupView()
        
    }
    private func setupView() {
        stackView.addArrangedSubview(busButton)
        stackView.addArrangedSubview(airPlaneButton)
        
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            busButton.heightAnchor.constraint(equalToConstant: 50),
            
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
        
    }
    
    @objc private func didTappedButton(_ sender: UIButton) {
        switch sender.tag {
        case 1: // bus
            let controller = BusViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case 2: // Air
            break
        default:
            break
        }
    }
}

class BusViewController: UIViewController {
    

    var list: [BusServiceList] = []
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 60
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Bus Services"
//        navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .white
        setupView()
        didTappedAuth()
        let loadButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(didTappedAuth))
        navigationItem.rightBarButtonItems = [loadButton]
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    
    @objc func didTappedAuth() {
        self.callAuthApi()
    }
    
   
}

// Auth API
extension BusViewController  {
    func callAuthApi() {
        
        NetworkClass.shared.fetchBusTransportData(service: "get_transport_services", parameters: nil) { (response: [BusServiceList]!, hasError: String?) in
            if let error = hasError {
                print(error)
                return
            }
            self.list = response
            self.tableView.reloadData()
        }
    }
}

// flight search Api
extension BusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = list[indexPath.row].service_name
        cell.textLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        cell.imageView?.image = UIImage(named: "ic_bus")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = list[indexPath.row]
        let controller = DepartureCitiesController()
        let model = BusRequestModel(service: data)
        controller.busRequestModel = model
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

