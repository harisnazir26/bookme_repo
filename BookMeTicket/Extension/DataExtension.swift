//
//  DataExtension.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation

extension Data {
    var prettyPrintedJSONString: NSString? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }
        print(prettyPrintedString)
        return prettyPrintedString
    }
}

extension Date {
    var currentDateString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let date = formatter.string(from: self)
        return date
    }
}
