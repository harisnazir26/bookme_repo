//
//  NetworkClass.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation
import Alamofire

enum URLs: String {
    case busURL = "https://bookme.pk/REST/API/rest_api.php/"
    case airLineURL = "https://bookme.pk/REST/API/flight.php/"
}


class NetworkClass {
    static let shared = NetworkClass()
    private init() {}
    
    func fetchBusTransportData<T: Decodable>(service: String, parameters: NSDictionary?, completion: @escaping (T?, _ hasError: String?) -> ()) {
        Spinner.start()
        var param = [
            "api_key": "a9d7990ca3ae9d97088b5924acbf8046",
        ]
        if let parameter = parameters {
            for (key, val) in parameter {
                param[key as! String] = val as? String
            }
        }
        let header: HTTPHeaders = ["Authorization" : "Basic 884a5d7b112251921e02d0719a48de79e7c4d1a70e3391408756621ffaf8e791"]
        guard var url = URL(string: "\(URLs.busURL.rawValue)\(service)") else {
            print("base url in valid")
            completion(nil, "Invalid URL")
            Spinner.stop()
            return
        }
        if service == "buses/saveOrders" {
            url = URL(string: "https://bookme.pk/api/v2/\(service)") ?? url
        }
        printParameters(paramters: param as NSDictionary)
        AF.request(url, method: .post, parameters: param, encoder: JSONParameterEncoder.default, headers: header)
            .responseData { responseData in
                print("URL: ", url.absoluteString)
                switch responseData.result {
                    
                case .success(let data):
                    print("\n========== Response ==========")
                    _ = data.prettyPrintedJSONString
                    
                    do {
                        let decodedObject = try JSONDecoder().decode(T.self, from: data)
                        completion(decodedObject, nil)
                    } catch let error as NSError {
                        print(error)
                        completion(nil, error.localizedDescription)
                    }
                    
                    print("==============================\n")
                case .failure(let error):
                    print("\n========== Error ==========")
                    print(error.localizedDescription)
                    print("==============================\n")
                    completion(nil, error.localizedDescription)
                }
                Spinner.stop()
            }
    }
    
    
    func printParameters(paramters: NSDictionary){
        print("============== Parameters ================\n")
        for (key, value) in paramters{
            print(key, ":", value)
        }
    }
}

extension NetworkClass {
    
    func fetchAirlineData<T: Decodable>(service: String, parameters: NSDictionary?, completion: @escaping (T?, _ hasError: String?) -> ()) {
        Spinner.start()
        var param = [
            "api_key": "a9d7990ca3ae9d97088b5924acbf8046",
        ]
        if let parameter = parameters {
            for (key, val) in parameter {
                param[key as! String] = val as? String
            }
        }
        let header: HTTPHeaders = ["Authorization" : "Basic 884a5d7b112251921e02d0719a48de79e7c4d1a70e3391408756621ffaf8e791"]
        guard var url = URL(string: "\(URLs.airLineURL.rawValue)\(service)") else {
            print("base url in valid")
            completion(nil, "Invalid URL")
            Spinner.stop()
            return
        }
        printParameters(paramters: param as NSDictionary)
        AF.request(url, method: .post, parameters: param, encoder: JSONParameterEncoder.default, headers: header)
            .responseData { responseData in
                print("URL: ", url.absoluteString)
                switch responseData.result {
                    
                case .success(let data):
                    print("\n========== Response ==========")
                    _ = data.prettyPrintedJSONString
                    
                    do {
                        let decodedObject = try JSONDecoder().decode(T.self, from: data)
                        completion(decodedObject, nil)
                    } catch let error as NSError {
                        print(error)
                        completion(nil, error.localizedDescription)
                    }
                    
                    print("==============================\n")
                case .failure(let error):
                    print("\n========== Error ==========")
                    print(error.localizedDescription)
                    print("==============================\n")
                    completion(nil, error.localizedDescription)
                }
                Spinner.stop()
            }
    }
    
    
}
