//
//  Spinner.swift
//  BookMeTicket
//
//  Created by Haris on 15/05/2024.
//

import UIKit

open class Spinner {
    
    private static var blockingView: UIView?
    private static let shared = Spinner()
    public static func start() {
        _ = Spinner.shared
        if blockingView == nil, let window = UIWindow.key {
            let frame = UIScreen.main.bounds
            
            blockingView = UIView(frame: frame)
            blockingView?.backgroundColor = .black.withAlphaComponent(0.5)
            
            window.addSubview(blockingView!)
        }
    }
    public static func stop() {
        if blockingView != nil {
            blockingView?.removeFromSuperview()
            blockingView = nil
        }
    }
}

extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}

