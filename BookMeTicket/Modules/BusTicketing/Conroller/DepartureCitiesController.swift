//
//  DepartureCitiesController.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import UIKit
import Alamofire


class DepartureCitiesController: UIViewController {
    
    var busRequestModel: BusRequestModel?
    var list: [DepartureCity] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 60
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Departure Cities"
        navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .white
        setupView()
        didTappedAuth()
        let loadButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(didTappedAuth))
        navigationItem.rightBarButtonItems = [loadButton]
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    
    @objc func didTappedAuth() {
        self.callAuthApi()
    }
    
   
}

extension DepartureCitiesController  {
    func callAuthApi() {
        let param = ["service_id" : self.busRequestModel?.service?.service_id ?? ""] as NSDictionary
        NetworkClass.shared.fetchBusTransportData(service: "get_deperature_cities", parameters: param) { (response: [DepartureCity]!, hasError: String?) in
            if let error = hasError {
                print(error)
                return
            }
            self.list = response
        }
    }
}

extension DepartureCitiesController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = list[indexPath.row].origin_city_name
        cell.textLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        cell.imageView?.image = UIImage(systemName: "airplane.departure")?.withTintColor(.darkGray)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = list[indexPath.row]
        let controller = DestinationCitiesController()
        self.busRequestModel?.departureCity = data
        controller.busRequestModel = self.busRequestModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

