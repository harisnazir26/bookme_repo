//
//  ServiceTransportTimeController.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import UIKit

class ServiceTransportTimeController: UIViewController {
    
    var busRequestModel: BusRequestModel?
    var list: [Times] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 60
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Bus Time"
        navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .white
        setupView()
        didTappedAuth()
        let loadButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(didTappedAuth))
        navigationItem.rightBarButtonItems = [loadButton]
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    
    @objc func didTappedAuth() {
        self.callAuthApi()
    }
    
   
}

extension ServiceTransportTimeController  {
    func callAuthApi() {
        
        let param = ["service_id" : self.busRequestModel?.service?.service_id ?? "",
                     "departure_city_id" : self.busRequestModel?.departureCity?.origin_city_id ?? "",
                     "arrival_city_id" : self.busRequestModel?.destinatonCity?.destination_city_id ?? "",
                     "date" : Date().currentDateString] as NSDictionary
        
        NetworkClass.shared.fetchBusTransportData(service: "bus_times", parameters: param) { (response: BussTime!, hasError: String?) in
            if let error = hasError {
                print(error)
                return
            }
            self.list = response.times ?? []
        }
    }
}

extension ServiceTransportTimeController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.numberOfLines = 0
        let string = "\(list[indexPath.row].arrival_terminal_address ?? "" )\n\n\(list[indexPath.row].departureDate ?? "") - \(list[indexPath.row].time ?? "")"
        cell.textLabel?.text = string
        cell.textLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = list[indexPath.row]
        let controller = SeatInfoController()
        self.busRequestModel?.transportTime = data
        controller.busRequestModel = self.busRequestModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


