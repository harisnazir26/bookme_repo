//
//  SeatInfoController.swift
//  BookMeTicket
//
//  Created by Haris on 15/05/2024.
//

import UIKit

class SeatInfoController: UIViewController {
    
    var busRequestModel: BusRequestModel?
    var list: [[Seat]] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 60
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Seat Information"
        navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .white
        setupView()
        didTappedAuth()
        let loadButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(didTappedAuth))
        navigationItem.rightBarButtonItems = [loadButton]
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    
    @objc func didTappedAuth() {
        self.callAuthApi()
    }
    
   
}

extension SeatInfoController  {
    func callAuthApi() {
        
        let param = ["service_id" : self.busRequestModel?.service?.service_id ?? "",
                     "departure_city_id" : self.busRequestModel?.departureCity?.origin_city_id ?? "",
                     "arrival_city_id" : self.busRequestModel?.destinatonCity?.destination_city_id ?? "",
                     "date" : Date().currentDateString,
                     "deptime" : "",
                     "time_id" : self.busRequestModel?.transportTime?.time_slot_id ?? 0,
                     "schedule_id" : self.busRequestModel?.transportTime?.schedule_id ?? 0,
                     "route_id" : self.busRequestModel?.transportTime?.route_id ?? 0,
                     "seats" : 2,] as NSDictionary
        
        NetworkClass.shared.fetchBusTransportData(service: "seats_info", parameters: param) { (response: SeatInfoModel!, hasError: String?) in
            if let error = hasError {
                print(error)
                return
            }
            self.list = response.seatplan?.seat ?? []
        }
    }
    
    func callBookingApi() {
        let param = ["bookings[0].origin_city _id" : self.busRequestModel?.departureCity?.origin_city_id ?? "",
                          "bookings[0].arrival_city_id" : self.busRequestModel?.destinatonCity?.destination_city_id ?? "",
                          "bookings[0].date" : Date().currentDateString,
                          "bookings[0].deptime" : self.busRequestModel?.transportTime?.time ?? "",
                          "bookings[0].route_id" : self.busRequestModel?.transportTime?.route_id ?? 0,
                          "bookings[0].time_id" : self.busRequestModel?.transportTime?.time_slot_id ?? 0,
                          "bookings[0].schedule_id" : self.busRequestModel?.transportTime?.schedule_id ?? 0,
                          "bookings[0].number_of_seats" : "1",
                          "bookings[0].ticket_price" : self.busRequestModel?.transportTime?.fare ?? 0,
                          "bookings[0].total_price" : self.busRequestModel?.transportTime?.fare ?? 0,
                          "bookings[0].seat_numbers_male" : "1",
                          "bookings[0].seat_numbers_female" : "0",
                          "bookings[0].service_id" : self.busRequestModel?.service?.service_id ?? "",
                          "name" : "Haris Nazir",
                          "email" : "harisnazir26@gmail.com",
                          "phone" : "03125133368",
                          "cnic" : "3740548538611",
                          "city" : "",
                          "address" : "",] as NSDictionary
        
        NetworkClass.shared.fetchBusTransportData(service: "buses/saveOrders", parameters: param) { (response: SeatInfoModel!, hasError: String?) in
            if let error = hasError {
                print(error)
                return
            }
//            self.list = response.seatplan?.seat ?? []
        }
    }
}

extension SeatInfoController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        list.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.numberOfLines = 0
        let string = list[indexPath.section][indexPath.row].seat_name ?? ""
        cell.textLabel?.text = string
        cell.textLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section)"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = list[indexPath.section][indexPath.row]
        self.busRequestModel?.seatInfo = data
        showConfirmPopUp()
        //        let controller = DestinationCitiesController()
//        controller.busRequestModel = self.busRequestModel
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showConfirmPopUp() {
        let alert = UIAlertController(title: "Confirmation", message: "Confirm Booking?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { _ in
            self.callBookingApi()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        self.present(alert, animated: true)
    }
    
}
