//
//  BusTime.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation

struct BussTime : Codable {
    let times : [Times]?
}

struct Tags : Codable {
    let name : String?
    let color : String?
    let description : String?
}

struct Facilities : Codable {
    let id : Int?
    let name : String?
    let img : String?
}

struct Times : Codable {
    let vaccinated_seats : Int?
    let dep_url : String?
    let departure_terminal_address : String?
    let route_id : Int?
    let bustype : String?
    let time_id : Int?
    let max_seats_sellection : Int?
    let arr_url : String?
    let arrival_terminal_lng : String?
    let contactlessonboarding : String?
    let schedule_id : Int?
    let seats : Int?
    let busname : String?
    let thumb : String?
    let departure_slug : String?
    let duration : String?
    let flexifare : Int?
    let female_seats : Int?
    let arrival_terminal_lat : String?
    let arrival_slug : String?
    let departure_terminal_id : Int?
    let departure_city_id : Int?
    let time_slot : String?
    let departure_terminal_lat : String?
    let service_priority : Int?
    let facilities : [Facilities]?
    let stops : [String]?
    let departureDate : String?
    let handling_charges : Int?
    let reservation_expiry_time : Int?
//    let available_seats : String?
    let arrival_terminal_address : String?
    let loyalty_points : Int?
    let tags : [Tags]?
    let original_fare : Int?
    let btype_id : Int?
    let today_offer : Bool?
    let is_connecting : Bool?
    let membership : String?
    let service_id : Int?
    let service_name : String?
    let departure_city_name : String?
    let arrival_city_name : String?
    let fare : Int?
    let arrival_terminal_id : Int?
    let arrtime : String?
    let time : String?
    let rating : Double?
    let arrival_city_id : Int?
    let departure_terminal_lng : String?
    let time_slot_id : Int?

}
