//
//  SeatInfoModel.swift
//  BookMeTicket
//
//  Created by Haris on 15/05/2024.
//

import Foundation
struct SeatInfoModel : Codable {
    let total_occupied : Int?
    let reserved_seats_male : String?
    let total_reserved : Int?
    let classes : [String]?
    let occupied_seats_female : String?
    let total_available : Int?
    let occupied_seats_male : String?
    let total_seats : Int?
    let seatplan : Seatplan?
    let customize : Bool?
    let available_seats : String?
    let vaccinated_seats : String?
    let reserved_seats_female : String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_occupied = try values.decodeIfPresent(Int.self, forKey: .total_occupied)
        reserved_seats_male = try values.decodeIfPresent(String.self, forKey: .reserved_seats_male)
        total_reserved = try values.decodeIfPresent(Int.self, forKey: .total_reserved)
        classes = try values.decodeIfPresent([String].self, forKey: .classes)
        occupied_seats_female = try values.decodeIfPresent(String.self, forKey: .occupied_seats_female)
        total_available = try values.decodeIfPresent(Int.self, forKey: .total_available)
        occupied_seats_male = try values.decodeIfPresent(String.self, forKey: .occupied_seats_male)
        total_seats = try values.decodeIfPresent(Int.self, forKey: .total_seats)
        seatplan = try values.decodeIfPresent(Seatplan.self, forKey: .seatplan)
        customize = try values.decodeIfPresent(Bool.self, forKey: .customize)
        available_seats = try values.decodeIfPresent(String.self, forKey: .available_seats)
        vaccinated_seats = try values.decodeIfPresent(String.self, forKey: .vaccinated_seats)
        reserved_seats_female = try values.decodeIfPresent(String.self, forKey: .reserved_seats_female)
    }

}

struct Seatplan : Codable {
    let seats : Int?
    let cols : Int?
    let rows : Int?
    let is_sleeper_bus : Int?
    let seat : [[Seat]]?

    enum CodingKeys: String, CodingKey {

        case seats = "seats"
        case cols = "cols"
        case rows = "rows"
        case is_sleeper_bus = "is_sleeper_bus"
        case seat = "seatplan"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        seats = try values.decodeIfPresent(Int.self, forKey: .seats)
        cols = try values.decodeIfPresent(Int.self, forKey: .cols)
        rows = try values.decodeIfPresent(Int.self, forKey: .rows)
        is_sleeper_bus = try values.decodeIfPresent(Int.self, forKey: .is_sleeper_bus)
        seat = try values.decodeIfPresent([[Seat]].self, forKey: .seat)
    }

}

struct Seat : Codable {
    let seat_id : Int?
    let seat_female : Bool?
    let seat_type : Int?
    let seat_color : String?
    let seat_name : String?
    let seat_ristrictmale : Bool?
    let label : String?
    let seat_pos : String?

    enum CodingKeys: String, CodingKey {

        case seat_id = "seat_id"
        case seat_female = "seat_female"
        case seat_type = "seat_type"
        case seat_color = "seat_color"
        case seat_name = "seat_name"
        case seat_ristrictmale = "seat_ristrictmale"
        case label = "label"
        case seat_pos = "seat_pos"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        seat_id = try values.decodeIfPresent(Int.self, forKey: .seat_id)
        seat_female = try values.decodeIfPresent(Bool.self, forKey: .seat_female)
        seat_type = try values.decodeIfPresent(Int.self, forKey: .seat_type)
        seat_color = try values.decodeIfPresent(String.self, forKey: .seat_color)
        seat_name = try values.decodeIfPresent(String.self, forKey: .seat_name)
        seat_ristrictmale = try values.decodeIfPresent(Bool.self, forKey: .seat_ristrictmale)
        label = try values.decodeIfPresent(String.self, forKey: .label)
        seat_pos = try values.decodeIfPresent(String.self, forKey: .seat_pos)
    }

}
