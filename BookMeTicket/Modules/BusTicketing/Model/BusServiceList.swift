//
//  BusServiceList.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation

struct BusServiceList : Codable {
    let careem : String?
    let phone : String?
    let facilities : String?
    let service_name : String?
    let service_id : String?
    let c_phone : String?
    let address : String?
    let reservation_expiry_time : Int?
    let slug : String?
    let flexifare : String?
    let schedule_start_date : String?
    let thumbnail : String?
    let cod : String?
    let schedule_end_date : String?
    let status : String?
    let offer_text : String?

}
