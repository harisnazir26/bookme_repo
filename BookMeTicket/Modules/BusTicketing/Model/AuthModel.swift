//
//  AuthModel.swift
//  SastaTicket
//
//  Created by Haris on 09/05/2024.
//

import Foundation

struct AuthModel: Codable {
    let success: Bool?
    let message: String?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let token: String?
    let iat, exp: Int?
}
