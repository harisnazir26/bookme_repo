//
//  DepartureCity.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation

struct DepartureCity : Codable {
    let origin_city_id : String?
    let short_name : String?
    let lat : String?
    let lng : String?
    let origin_city_name : String?
}

struct DestinationCity : Codable {
    let short_name : String?
    let destination_city_id : String?
    let lat : String?
    let lng : String?
    let destination_city_name : String?
}
