//
//  BusRequestModel.swift
//  BookMeTicket
//
//  Created by Haris on 14/05/2024.
//

import Foundation


struct BusRequestModel {
    var service: BusServiceList?
    var departureCity: DepartureCity?
    var destinatonCity: DestinationCity?
    var transportTime: Times?
    var seatInfo: Seat?
}
