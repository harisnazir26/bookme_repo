//
//  SearchRequestModel.swift
//  SastaTicket
//
//  Created by Haris on 09/05/2024.
//

import Foundation

struct SearchRequestModel : Codable {
    internal init(cabin_class: Cabin_class? = nil, flexibility: String? = nil, legs: [Legs]? = nil, non_stop_flight: Bool? = nil, traveler_count: Traveler_count? = nil, route_type: String? = nil) {
        self.cabin_class = cabin_class
        self.flexibility = flexibility
        self.legs = legs
        self.non_stop_flight = non_stop_flight
        self.traveler_count = traveler_count
        self.route_type = route_type
    }
    
    let cabin_class : Cabin_class?
    let flexibility : String?
    let legs : [Legs]?
    let non_stop_flight : Bool?
    let traveler_count : Traveler_count?
    let route_type : String?
}
struct Traveler_count : Codable {
    let num_adult : Int?
    let num_child : Int?
    let num_infant : Int?
}
struct Legs : Codable {
    let origin : String?
    let destination : String?
    let departure_date : String?
}
struct Cabin_class : Codable {
    let code : String?
    let label : String?
}
